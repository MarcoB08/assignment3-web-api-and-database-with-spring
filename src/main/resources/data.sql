-- Insert data in the franshise table --
INSERT INTO franchise (name, description) VALUES ('Harry Potter World', 'Harry Potter is een filmserie gebaseerd op de gelijknamige romans van J.K. Rowling. De serie wordt geproduceerd en gedistribueerd door Warner Bros. Pictures en bestaat uit acht fantasiefilms, beginnend met Harry Potter en de Steen der Wijzen en eindigend met Harry Potter en de Relieken van de Dood – Deel 2.');
INSERT INTO franchise (name, description) VALUES ('Indiana Jones', 'Indiana Jones is een Amerikaanse mediafranchise gebaseerd op de avonturen van Dr. Henry Walton "Indiana" Jones, Jr., een fictieve hoogleraar archeologie, die in 1981 begon met de film Raiders of the Lost Ark. In 1984 verscheen een prequel, The Temple of Doom, werd uitgebracht, en in 1989 een vervolg, The Last Crusade.');
INSERT INTO franchise (name, description) VALUES ('The Lord Of The Rings', 'The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel The Lord of the Rings');

-- Insert data in the movie table --
INSERT INTO movie (title, "genre", releaseyear, "director", "picture", "trailer", "franchise_id") VALUES ('Harry Potter and the Philosophers Stone','Fantasy-film', '2001/11/22','Chris Columbus', 'http://t1.gstatic.com/licensed-image?q=tbn:ANd9GcSQKRtJyq-DP8K3Fwc-28GEXE-J5viHUoXgRl0gK_rk-M9ISmlQMkZd9Ytt-x669BAC', 'https://www.youtube.com/watch?v=VyHV0BRtdxo', 1);
INSERT INTO movie (title, genre, releaseyear, director, picture, trailer, franchise_id) VALUES ('The Lord of the Rings: The Two Towers','Fantasy-film', '2002/12/18','Peter Jackson', 'https://images.pathe-thuis.nl/25049_450x640.jpg', 'https://www.youtube.com/watch?v=LbfMDwc4azU', 3);
INSERT INTO movie (title, genre, releaseyear, director, picture, trailer, franchise_id) VALUES ('Indiana Jones and the Last Crusade','Adventure', '1989/09/19','Steven Spielberg', 'https://thumbnails.cbsig.net/CBS_Production_Entertainment_VMS/2020/12/08/1829046851826/IJLC_SAlone_THM_16.9_1920x1080_1185815_1920x1080.jpg', 'https://www.youtube.com/watch?v=DKg36LBVgfg', 2);

-- Insert data in the character table --
INSERT INTO character (name, alias, gender, picture) VALUES ('Harry Potter','Daniel Radcliffe', 'Wizard', 'https://cdn.britannica.com/43/237443-050-C81087AD/Daniel-Radcliffe-2022-London-England.jpg');
INSERT INTO character (name, alias, gender, picture) VALUES ('Jones jr','Harrison Ford', 'Archaeology', 'https://upload.wikimedia.org/wikipedia/commons/3/34/Harrison_Ford_by_Gage_Skidmore_3.jpg');
INSERT INTO character (name, alias, gender, picture) VALUES ('Gandalf the Grey','Ian McKellen', 'Wizard', 'https://image.tmdb.org/t/p/w500/5cnnnpnJG6TiYUSS7qgJheUZgnv.jpg');


-- Insert data in the movie_character table --
INSERT INTO movie_character (movies_id, character_id) VALUES (1,1);
INSERT INTO movie_character (movies_id, character_id) VALUES (2,3);
INSERT INTO movie_character (movies_id, character_id) VALUES (3,2);