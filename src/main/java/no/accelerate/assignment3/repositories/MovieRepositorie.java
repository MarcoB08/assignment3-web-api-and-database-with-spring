package no.accelerate.assignment3.repositories;

import no.accelerate.assignment3.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepositorie extends JpaRepository<Movie,Integer> {}
