package no.accelerate.assignment3.repositories;

import no.accelerate.assignment3.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepositorie extends JpaRepository<Franchise,Integer> {}
