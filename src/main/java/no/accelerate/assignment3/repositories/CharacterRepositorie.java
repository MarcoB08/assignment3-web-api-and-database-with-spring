package no.accelerate.assignment3.repositories;

import no.accelerate.assignment3.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepositorie extends JpaRepository<Character, Integer> {}
