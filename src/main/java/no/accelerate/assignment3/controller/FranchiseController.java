package no.accelerate.assignment3.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.accelerate.assignment3.models.DTO.franchise.FranchiseDTO;
import no.accelerate.assignment3.models.DTO.franchise.FranchisePostDTO;
import no.accelerate.assignment3.models.DTO.franchise.FranchisePutDTO;
import no.accelerate.assignment3.models.DTO.movie.MovieDTO;
import no.accelerate.assignment3.models.Franchise;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.service.franchise.FranchiseServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/*
This controller is a CRUD enpoint of Franchise
 */

@RestController
@RequestMapping("/franchise")
public class FranchiseController {
    @Autowired
    private FranchiseServiceImpl franchiseService;

    private ModelMapper mapper;

    public FranchiseController() {
        mapper=new ModelMapper();
    }

    @GetMapping("/getall")
    @Operation(summary = "Gets all franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation =FranchiseDTO.class))
                            )}
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class)
                            ),}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class)
                            ),}
            )
    })
    public Collection<FranchiseDTO> getAllFranchise(){
        Collection<Franchise> f = franchiseService.findAll();
        Collection<FranchiseDTO> c=new ArrayList();

        for (Franchise franchise: f){
            FranchiseDTO franchiseDTO=new FranchiseDTO();
            mapper.map(franchise,franchiseDTO);
            c.add(franchiseDTO);
        }

        return c;
    }

    @GetMapping("{id}")
    @Operation(summary = "Gets a franchise by their id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = FranchiseDTO.class)
                            ),}
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class)
                            ),}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class)
                            ),}
            )

    })
    public FranchiseDTO getFranchiseById(@PathVariable int id){
        Franchise franchise=franchiseService.findById(id);
        FranchiseDTO franchiseDTO= new FranchiseDTO();
        mapper.map(franchise,franchiseDTO);
        return franchiseDTO;
    }

    @PostMapping
    @Operation(summary = "Adds a new franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity createFranchise(@RequestBody FranchisePostDTO franchisePostDTO){
        Franchise franchise= new Franchise();
        mapper.map(franchisePostDTO,franchise);
        franchiseService.add(franchise);
        URI location = URI.create("/franchise");
        return ResponseEntity.created(location).build();
    }

    @PutMapping()
    @Operation(summary = "Update franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity updateFranchise(@RequestBody FranchisePutDTO franchisePutDTO){
        Franchise franchise=new Franchise();
        mapper.map(franchisePutDTO,franchise);
        franchiseService.update(franchise);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a franchise")
    @ApiResponses(value ={
            @ApiResponse(
                    responseCode = "204",
                    description = "success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity deleteFranchise(@PathVariable int id){
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
