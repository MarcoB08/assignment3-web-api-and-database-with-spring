package no.accelerate.assignment3.controller;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.models.DTO.character.CharacterDTO;
import no.accelerate.assignment3.models.DTO.character.CharacterPostDTO;
import no.accelerate.assignment3.models.DTO.character.CharacterPutDTO;
import no.accelerate.assignment3.models.DTO.movie.MovieDTO;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.service.character.CharacterServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.util.*;

/*
This controller is CRUD enpoint of character
*/

@RestController
@RequestMapping("/character")
public class CharacterController {

    @Autowired
    private CharacterServiceImpl characterService;


    private final ModelMapper mapper;

    public CharacterController() {
        mapper=new ModelMapper();
    }

    @GetMapping("/getall")
    @Operation(summary = "Gets all characters")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation = CharacterDTO.class))
                            )}
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class)
                            ),}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class)
                            ),}
            )
    })
    public Collection<CharacterDTO> getAllCharacter(){
        Collection<Character> allCharacters1=characterService.findAll();
        Collection<CharacterDTO> allCharactersDTO=new ArrayList();

        for (Character character:allCharacters1){
            CharacterDTO characterDTO=new CharacterDTO();
            mapper.map(character,characterDTO);
            allCharactersDTO.add(characterDTO);
        }

        return  allCharactersDTO;
    }

    @GetMapping("/{id}")
    @Operation(summary = "Gets a character by their id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = CharacterDTO.class)
                            ),}
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class)
                            ),}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class)
                            ),}
            )

    })
    public CharacterDTO getCharacterById(@PathVariable String id){
        CharacterDTO characterDTO= new CharacterDTO();
        Character character=characterService.findById(Integer.parseInt(id));
        mapper.map(character,characterDTO);
        return characterDTO;
    }

    @PostMapping()
    @Operation(summary = "Adds a new character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity addNewCharacter(@RequestBody CharacterPostDTO characterPostDTO){
        Character character=new Character();
        mapper.map(characterPostDTO,character);
        characterService.add(character);
        URI location = URI.create("/character");
        return ResponseEntity.created(location).build();
    }

    @PutMapping()
    @Operation(summary = "Updates a character")
    @ApiResponses(value ={
            @ApiResponse(
                    responseCode = "204",
                    description = "success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity editCharacter(@RequestBody CharacterPutDTO characterPutDTO){
        Character character=new Character();
        mapper.map(characterPutDTO,character);
        characterService.update(character);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a character")
    @ApiResponses(value ={
            @ApiResponse(
                    responseCode = "204",
                    description = "success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity deleteCharacter(@PathVariable String id){
        characterService.deleteById(Integer.parseInt(id));
        return ResponseEntity.noContent().build();
    }

}
