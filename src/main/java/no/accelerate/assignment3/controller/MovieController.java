package no.accelerate.assignment3.controller;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.models.DTO.character.CharacterDTO;
import no.accelerate.assignment3.models.DTO.movie.*;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.service.movie.MovieService;
import org.modelmapper.ModelMapper;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/*
This controller is a CRUD enpoint of Movie
 */

@RestController
@RequestMapping(path = "/movie")
public class MovieController {
    private final MovieService movieService;
    private final ModelMapper mapper;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;
        mapper=new ModelMapper();
    }

    @GetMapping("/getall")
    @Operation(summary = "Gets all movies")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation = MovieDTO.class))
                            )}
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class)
                            ),}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class)
                            ),}
            )
    })
    public Collection<MovieDTO> getAllmovies(){
        Collection<Movie> allMovies = movieService.findAll();
        Collection<MovieDTO> allMoviesDTO = new ArrayList<>();

        for (Movie movie : allMovies){
            MovieDTO movieDTO = new MovieDTO();
            mapper.map(movie,movieDTO);
            allMoviesDTO.add(movieDTO);
        }
        return allMoviesDTO;
    }

    @GetMapping("{id}")
    @Operation(summary = "Gets a movie by their id")
    @ApiResponses(value = {
        @ApiResponse(
                responseCode = "200",
                description = "success",
                content = {
                        @Content(mediaType = "application/json",
                        schema = @Schema(implementation = MovieDTO.class)
                        ),}
        ),
        @ApiResponse(
                responseCode = "400",
                description = "Bad Request",
                content = {
                        @Content(mediaType = "application/json",
                                schema = @Schema(implementation = ProblemDetail.class)
                        ),}
        ),
        @ApiResponse(
                responseCode = "404",
                description = "Not Found",
                content = {
                        @Content(mediaType = "application/json",
                                schema = @Schema(implementation = ProblemDetail.class)
                        ),}
        )

    })
    public GetMovieById getMovieById(@PathVariable int id){
        GetMovieById movieDTO = new GetMovieById();
        Movie movie = movieService.findById(id);

        Set<Integer> characterId = new HashSet<>();

        for(Character character : movie.getCharacter()){
            characterId.add(character.getId());
        }
        mapper.map(movie,movieDTO);
        movieDTO.setFranchiseid(movie.getFranchise().getId());
        movieDTO.setCharacterid(characterId);
        return movieDTO;
    }

    @PostMapping
    @Operation(summary = "Adds a new movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity addNewMovie(@RequestBody MoviePostDTO moviePostDTO){
        Movie movie = new Movie();
        mapper.map(moviePostDTO, movie);
        movieService.add(movie);
        URI location = URI.create("/movie");
        return ResponseEntity.created(location).build();
    }

    @PostMapping("/{id}/addcharacter")
    @Operation(summary = "Adds a character to movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity addNewCharactersToMovie(@RequestBody AddCharacterToMovieDTO addCharacterToMovieDTO,@PathVariable int id){
        movieService.addCharacter(id,addCharacterToMovieDTO.getCharacterIds());
        URI location = URI.create("/movie/"+id+"/addcharacter");
        return ResponseEntity.created(location).build();
    }

    @PostMapping("/{id}/addfranchise/{fid}")
    @Operation(summary = "Adds a franchise to movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity addNewFranchiseToMovie(@PathVariable int id,@PathVariable int fid){
        movieService.addFranchise(id,fid);
        URI location = URI.create("/movie/"+id+"/addfranchise");
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}/updatefranchise{fid}")
    @Operation(summary = "Updates franchise to movie")
    @ApiResponses(value ={
            @ApiResponse(
                    responseCode = "204",
                    description = "success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity updateFranchiseToMovie(@PathVariable int id,@PathVariable int fid){
        movieService.addFranchise(id,fid);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}/updatecharacter")
    @Operation(summary = "Updates a character to movie")
    @ApiResponses(value ={
            @ApiResponse(
                    responseCode = "204",
                    description = "success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity updateCharactersToMovie(@RequestBody AddCharacterToMovieDTO addCharacterToMovieDTO,@PathVariable int id){
        movieService.addCharacter(id,addCharacterToMovieDTO.getCharacterIds());
        return ResponseEntity.noContent().build();
    }

    @PutMapping
    @Operation(summary = "Updates a movie")
    @ApiResponses(value ={
        @ApiResponse(
                responseCode = "204",
                description = "success",
                content = @Content
        ),
        @ApiResponse(
                responseCode = "400",
                description = "Bad Request",
                content = @Content
        ),
        @ApiResponse(
                responseCode = "404",
                description = "Not Found",
                content = @Content
        )
    })
    public ResponseEntity editMovie(@RequestBody MoviePutDTO moviePutDTO){
        Movie movie = new Movie();
        mapper.map(moviePutDTO,movie);
        movieService.update(movie);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a movie")
    @ApiResponses(value ={
            @ApiResponse(
                    responseCode = "204",
                    description = "success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity deleteMovie(@PathVariable int id){
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }



}
