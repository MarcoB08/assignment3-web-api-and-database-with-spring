package no.accelerate.assignment3.service.franchise;

import jakarta.persistence.EntityNotFoundException;
import no.accelerate.assignment3.models.Franchise;
import no.accelerate.assignment3.repositories.FranchiseRepositorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class FranchiseServiceImpl implements FranchiseService {

    @Autowired
    private FranchiseRepositorie franchiseRepositorie;

    //this method will return a franchise entity by its id
    @Override
    public Franchise findById(Integer integer) {
        return franchiseRepositorie.findById(integer).orElseThrow(()->new EntityNotFoundException("could not find franchise"));
    }

    //this method will return a list of all franchise
    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepositorie.findAll();
    }

    //this method will create a new franchise
    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepositorie.save(entity);
    }

    //this method will update existin franchise
    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepositorie.save(entity);
    }

    //this method will delete a franchise by the given id
    @Override
    public void deleteById(Integer integer) {
        franchiseRepositorie.deleteById(integer);
    }
}
