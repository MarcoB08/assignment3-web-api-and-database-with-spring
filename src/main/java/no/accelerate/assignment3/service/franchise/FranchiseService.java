package no.accelerate.assignment3.service.franchise;

import no.accelerate.assignment3.models.Franchise;
import no.accelerate.assignment3.service.CrudService;


public interface FranchiseService extends CrudService<Franchise,Integer> {
}
