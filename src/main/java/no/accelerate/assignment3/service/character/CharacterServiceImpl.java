package no.accelerate.assignment3.service.character;

import jakarta.persistence.EntityNotFoundException;
import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.repositories.CharacterRepositorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CharacterServiceImpl implements CharacterService{

    @Autowired
    private CharacterRepositorie characterRepositorie;

    //this method will return a character entity by its id
    @Override
    public Character findById(Integer integer) {
        return characterRepositorie.findById(integer).orElseThrow(()->new EntityNotFoundException("Character not found"));
    }

    //this method will return a list of character
    @Override
    public Collection<Character> findAll() {
        return characterRepositorie.findAll();
    }

    //this method will create a new Character
    @Override
    public Character add(Character entity) {
        return characterRepositorie.save(entity);
    }

    //this method will update existin character
    @Override
    public Character update(Character entity) {
        return characterRepositorie.save(entity);
    }

    //this method will delete a character by its id
    @Override
    public void deleteById(Integer integer) {
        characterRepositorie.deleteById(integer);
    }
}
