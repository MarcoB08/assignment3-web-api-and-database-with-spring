package no.accelerate.assignment3.service.character;

import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.service.CrudService;

public interface CharacterService extends CrudService<Character,Integer> {

}
