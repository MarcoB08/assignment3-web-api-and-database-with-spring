package no.accelerate.assignment3.service.movie;

import jakarta.persistence.EntityNotFoundException;
import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.models.Franchise;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.repositories.MovieRepositorie;
import no.accelerate.assignment3.service.character.CharacterServiceImpl;
import no.accelerate.assignment3.service.franchise.FranchiseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class MovieServiceImpl implements MovieService{
    private final MovieRepositorie movieRepositorie;

    @Autowired
    private CharacterServiceImpl characterService;

    @Autowired
    private FranchiseServiceImpl franchiseService;

    public MovieServiceImpl(MovieRepositorie movieRepositorie) {
        this.movieRepositorie = movieRepositorie;
    }

    //this method will return a movie entity by its id
    @Override
    public Movie findById(Integer id) {
        return movieRepositorie.findById(id).orElseThrow(()->new EntityNotFoundException("could not find movie"));
    }

    //this method will return a list of all movies
    @Override
    public Collection<Movie> findAll() {
        return movieRepositorie.findAll();
    }

    //this method will create a new movie
    @Override
    public Movie add(Movie entity) {
        return movieRepositorie.save(entity);
    }

    //this method will update existin movie
    @Override
    public Movie update(Movie entity) {
        return movieRepositorie.save(entity);
    }

    //this method will delete a movie by its id
    @Override
    public void deleteById(Integer id) {
        movieRepositorie.deleteById(id);
    }

    //this method will add new list of character to a movie
    @Override
    public void addCharacter(int id, Set<Integer> characterIds) {
        Movie movie=findById(id);
        Set <Character> newCharacters=new HashSet();
        for (Integer characterId:characterIds){
            Character character=characterService.findById(characterId);
            newCharacters.add(character);
        }
        movie.setCharacter(newCharacters);
        movieRepositorie.save(movie);
    }

    //this method will update the list of character of a movie
    @Override
    public void updateCharacter(int id,Set<Integer> characterIds) {
        Movie movie=findById(id);
        Set <Character> newCharacters=new HashSet();
        for (Integer characterId:characterIds){
            Character character=characterService.findById(characterId);
            newCharacters.add(character);
        }
        movie.setCharacter(newCharacters);
        movieRepositorie.save(movie);
    }

    //this method will add a specific franchise to a movie
    @Override
    public void addFranchise(int id,int franchiseId) {
        Movie movie=findById(id);
        Franchise franchise=franchiseService.findById(franchiseId);
        movie.setFranchise(franchise);
    }

    //this method will update the franchise of a movie
    @Override
    public void UpdateFranchise(int id,int franchiseId) {
        Movie movie=findById(id);
        Franchise franchise=franchiseService.findById(franchiseId);
        movie.setFranchise(franchise);
    }
}
