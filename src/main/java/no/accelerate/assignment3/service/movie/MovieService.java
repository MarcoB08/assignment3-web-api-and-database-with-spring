package no.accelerate.assignment3.service.movie;

import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.service.CrudService;

import java.util.List;
import java.util.Set;

public interface MovieService extends CrudService<Movie, Integer> {
    void addCharacter(int id,Set<Integer> characterId);
    void updateCharacter(int id,Set<Integer> characterId);

    void addFranchise(int id,int franchiseId);

    void UpdateFranchise(int id,int franchiseId);

}
