package no.accelerate.assignment3.models;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 50)
    @NotNull
    private String name;

    @Column(length = 500)
    private String description;

    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;

}
