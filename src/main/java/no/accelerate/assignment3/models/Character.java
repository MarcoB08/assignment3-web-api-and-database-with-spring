package no.accelerate.assignment3.models;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 25, nullable = false)
    @NotNull
    private String name;

    @Column(length = 25)
    private String alias;

    @Column(length = 25)
    private String gender;

    @Column(length = 200)
    private String picture;

    @ManyToMany(mappedBy = "character")
    private Set<Movie> movies;
}
