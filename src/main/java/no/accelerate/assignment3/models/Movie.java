package no.accelerate.assignment3.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 500, nullable = false)
    @NotNull
    private String title;

    @Column(length = 25)
    @NotNull
    private String genre;

    private LocalDate releaseyear;

    @Column(length = 50)
    private String director;

    @Column(length = 250)
    private String picture;

    @Column(length = 250)
    private String trailer;

    //RelationShips
   @ManyToOne
    private Franchise franchise;

    @ManyToMany
    private Set<Character> character;


}
