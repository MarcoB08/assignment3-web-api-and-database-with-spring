package no.accelerate.assignment3.models.DTO.character;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CharacterPostDTO {
    private String name;
    private String alias;
    private String gender;
    private String picture;

}
