package no.accelerate.assignment3.models.DTO.movie;

import lombok.Getter;
import lombok.Setter;
import no.accelerate.assignment3.models.Franchise;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
public class GetMovieById {
    private int id;
    private String title;
    private String genre;
    private LocalDate releaseyear;
    private String director;
    private String picture;
    private String trailer;
    private Integer franchiseid;
    private Set<Integer> characterid;
}
