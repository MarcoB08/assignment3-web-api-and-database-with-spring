package no.accelerate.assignment3.models.DTO.franchise;

import lombok.Getter;
import lombok.Setter;
import no.accelerate.assignment3.models.Movie;

import java.util.Set;

@Getter
@Setter
public class FranchisePostDTO {
    private String name;
    private String description;
}
