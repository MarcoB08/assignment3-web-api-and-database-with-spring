package no.accelerate.assignment3.models.DTO.character;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CharacterPutDTO {
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String picture;
}
