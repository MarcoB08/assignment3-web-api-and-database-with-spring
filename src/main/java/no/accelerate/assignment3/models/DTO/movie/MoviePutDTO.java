package no.accelerate.assignment3.models.DTO.movie;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MoviePutDTO {
    private int id;
    private String title;
    private String genre;
    private LocalDate releaseyear;
    private String director;
    private String picture;
    private String trailer;
}

