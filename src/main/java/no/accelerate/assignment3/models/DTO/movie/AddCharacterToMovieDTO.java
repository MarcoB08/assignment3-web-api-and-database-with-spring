package no.accelerate.assignment3.models.DTO.movie;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class AddCharacterToMovieDTO {
   private Set<Integer> characterIds;
}
