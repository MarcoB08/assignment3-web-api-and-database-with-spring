package no.accelerate.assignment3.models.DTO.franchise;

import jakarta.persistence.Column;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;
import no.accelerate.assignment3.models.Movie;

import java.util.Set;

@Getter
@Setter
public class FranchiseDTO {
    private int id;
    private String name;
    private String description;
}
