package no.accelerate.assignment3.models.DTO.movie;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MovieListDTO {
    private int id;
    private String title;
    private String genre;
    private LocalDate releaseyear;
    private String director;
}
