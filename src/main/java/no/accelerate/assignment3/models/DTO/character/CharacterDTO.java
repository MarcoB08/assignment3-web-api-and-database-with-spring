package no.accelerate.assignment3.models.DTO.character;

import lombok.Getter;
import lombok.Setter;
import no.accelerate.assignment3.models.Movie;

import java.util.Set;

@Setter
@Getter
public class CharacterDTO {
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String picture;
}
