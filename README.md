# Assignment 3 Build a Web API and database with Spring

This project is a project made by Sahil Mankani and Marco Beckers. 
This project has integrate a Hybrate Spring database movielibary. Also we have integrate Swagger to test the Web API.

We made a mapstructure to structure the code 

## MapStructure

- controller
    - CharacterController
    - FranchiseController
    - MovieController
- models
    - DTO
        - Character
            - CharacterDTO
            - CharacterPostDTO
            - CharacterPutDTO
        - Franchise
            - FranchiseDTO
            - FranchisePostDTO
            - FranchisePutDTO
        - Movie 
            - MovieDTO
            - MoviePostDTO
            - MoviePutDTO
            - AddCharacterToMovieDTO
    - Character
    - Franchise
    - Movie
- repositories
    - CharacterRepositorie
    - FranchiseRepositorie
    - MovieRepositorie
- service
    - character
        - CharacterService
        - CharacterServiceImpl
    - franchise
        - FranchiseService
        - FranchiseServiceImpl
    - movie
        - MovieService
        - MovieServiceImpl
    - CrudService

### Use our project

- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/MarcoB08/assignment3-web-api-and-database-with-spring.git
git branch -M main
git push -uf origin main

```
#### Swagger

To test this project with Swagger you can go to this link http://localhost:8080/swagger-ui/index.html#/ 

